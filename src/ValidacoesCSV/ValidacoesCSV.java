package ValidacoesCSV;

import java.io.File;

import java.io.FileNotFoundException;

import java.util.Scanner;

public class ValidacoesCSV {

	public static void main(String[] args) {

		// caminho da pasta dos arquivos pendentes
		String fullPath = "Pendentes\\";

		File files = new File(fullPath);

		File filesList[] = files.listFiles();

		try {

			for (File arquivo : filesList) {

				// lendo todos CSV
				readCsv(arquivo, fullPath);

			}

		} catch (Exception e) {
			System.out.println(e);
		}

	}

	// lançando uma exceção
	public static void readCsv(File arquivo, String fullPath) throws FileNotFoundException {

		// scaner para ler todas as linhas
		Scanner sc = new Scanner(new File(fullPath + arquivo.getName()));

		// Diretorio Validados
		String fullPathValid = "validados\\";

		// Diretorio Invalidados
		String fullPathInvalid = "invalidos\\";

		File moveFile = new File(fullPath + arquivo.getName());
		
		boolean valid = true;
		
		//vericação se o arquivo ta vazio
		if (moveFile.length() == 0) {

			sc.close();
			
			//movendo o arquivo vazio para pasta invalido
			moveFile.renameTo(new File(fullPathInvalid + arquivo.getName()));
			

		} else {
			
			//verificação de linha
			while (sc.hasNext()) {
				String line = sc.nextLine();

				// line.Split para dividir as colunas após encontrar o caractere ";"
				String[] columns = line.split(";");

				
				//verifficando se as colunas são diferente a 4 e e ignorando linha vazia
				if (columns.length != 4 && !line.isEmpty()) {
					
					valid = false;

				}

			}

		}
		// fechar scanner
		sc.close();
		
		if (valid) {
			moveFile.renameTo(new File(fullPathValid + arquivo.getName()));
		} else {
			moveFile.renameTo(new File(fullPathInvalid + arquivo.getName()));
		}

	}

}
